package com.binghamton.DropAlert;



import android.app.Activity;
import android.os.Bundle;


public abstract class FallMonitorAbstActivity extends Activity {

	protected abstract void initializeUI();

	protected void onCreate(Bundle savedInstanceState) {
		initializeUI();
		super.onCreate(savedInstanceState);
	}

}